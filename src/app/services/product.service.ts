import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { API } from 'aws-amplify';
import { ProductModel, ProductData } from '../models/product.model';
import { CategoryModel, CategoryData } from '../models/category.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  products: BehaviorSubject<ProductModel>;
  category: BehaviorSubject<CategoryModel>;
  selectedProduct: BehaviorSubject<ProductData>;
  constructor() {
    this.products = new BehaviorSubject(null);
    this.category = new BehaviorSubject(null);
    this.selectedProduct = new BehaviorSubject(null);
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any[]> | Promise<any> | any[] {
    if (route.data.title === 'Products') {
      return this.loadProducts();
    } else {
      return this.loadCategories();
    }
  }

  loadProducts(): Promise<ProductModel> {
    return new Promise((resolve, reject) => {
      API.get('table', '/table/products', {}).then((result: any) => {
        this.products.next(result);
        resolve(result);
      })
        .catch((err) => {
          reject(err);
        });
    });
  }

  loadCategories(force?: boolean): Promise<CategoryModel> {
    return new Promise((resolve, reject) => {
      if (this.category.value && !force) {
        resolve(this.category.value);
      } else {
        API.get('table', '/table/category', {}).then((result: CategoryModel) => {
          this.category.next(result);
          resolve(result);
        });
      }
    });
  }

  async updateProduct(data: ProductData) {
    const id = data.id;
    delete (data.new);
    delete (data.id);
    const r = await API.post('table', `/table/products/${id}`, {
      body: data,
      headers: {}
    });
    return this.loadProducts();
  }

  async deleteProduct(data: ProductData) {
    const id = data.id;
    delete (data.new);
    delete (data.id);
    const r = await API.del('table', `/table/products/${id}`, {});
    return this.loadProducts();
  }

  async createProduct(data: ProductData) {
    delete (data.new);
    const r = await API.put('table', '/table/products', {
      body: data,
      headers: {}
    });
    return this.loadProducts();
  }

  async addCategory(name: string) {
    const r = await API.put('table', '/table/category', {
      body: {
        name
      },
      headers: {}
    });
    return this.loadCategories(true);
  }
}
