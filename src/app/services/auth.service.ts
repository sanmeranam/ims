import { Injectable, OnInit } from '@angular/core';
import { Auth } from 'aws-amplify';
import { BehaviorSubject, Observable } from 'rxjs';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements CanActivate {

  user: BehaviorSubject<any>;


  constructor(private router: Router) {
    Auth.currentUserInfo().then(user => {
      this.user = new BehaviorSubject(user);
    });
  }

  // tslint:disable-next-line: max-line-length
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    return Auth.currentUserInfo().then(u => {
      if (!u) {
        this.router.navigate(['/login']);
        return Promise.resolve(false);
      } else {
        return Promise.resolve(true);
      }
    });
  }

  async SingOut() {
    await Auth.signOut()
      .then(data => this.user.next(null));
  }

  async PasswordUpdate(user, newPassword) {
    return await Auth.completeNewPassword(
      user,
      newPassword,
      {}
    );
  }

  async SignIn(username: string, password: string) {
    try {
      const userData = await Auth.signIn(username, password);
      this.user.next(userData);
    } catch (err) {
      const user = { challengeName: 'ERROR', message: '' };
      if (err.code === 'UserNotConfirmedException') {
        user.message = 'User not verified';
      } else if (err.code === 'PasswordResetRequiredException') {
        user.message = 'User password expired. Reset require.';
      } else if (err.code === 'NotAuthorizedException') {
        user.message = 'User not authorized.';
      } else if (err.code === 'UserNotFoundException') {
        user.message = 'User not found.';
      }
      this.user.next(user);
    }
  }
}
