import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer'
  },
  {
    title: true,
    name: 'Invetory'
  },
  {
    name: 'Products',
    url: '/products',
    icon: 'icon-layers'
  },
  {
    name: 'Category',
    url: '/products/category',
    icon: 'icon-list'
  },
  {
    divider: true
  }
];
