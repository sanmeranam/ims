import { Component, OnInit, OnDestroy } from '@angular/core';
import { navItems } from '../../_nav';
import { AuthService } from 'src/app/services/auth.service';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';
import { ProductService } from 'src/app/services/product.service';
import { ProductData } from 'src/app/models/product.model';
import { CategoryModel } from 'src/app/models/category.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent implements OnInit, OnDestroy {

  public sidebarMinimized = false;
  public navItems = navItems;
  public user: any;
  public productSelected: ProductData;
  public category: CategoryModel;
  private userSub: Subscription;
  private prodSub: Subscription;
  public showAdd: boolean;

  constructor(private auth: AuthService, private route: Router, private prodServ: ProductService) { }

  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }

  showAside() {
    document.body.classList.add('aside-menu-lg-show');
  }

  hideAside() {
    document.body.classList.remove('aside-menu-lg-show');
  }

  async onSignOut() {
    await this.auth.SingOut();
    this.route.navigate(['/login']);
  }

  ngOnInit(): void {

    this.route.events.forEach((event) => {
      if (event instanceof NavigationEnd) {
        if (this.route.url === '/products') {
          this.showAdd = true;
        } else {
          this.showAdd = false;
        }
        this.hideAside();
      }
    });

    this.userSub = this.auth.user.subscribe(user => this.user = user);
    this.prodServ.loadCategories().then((result: CategoryModel) => {
      this.category = result;
    });
    this.prodSub = this.prodServ.selectedProduct.subscribe(p => {
      if (!p) {
        this.productSelected = null;
        this.hideAside();
        return;
      }
      this.productSelected = p;
      this.showAside();
    });
  }

  onAddNewProduct() {
    this.prodServ.selectedProduct.next(new ProductData(true));
  }

  deSelectProduct() {
    this.prodServ.selectedProduct.next(null);
  }

  ngOnDestroy(): void {
    this.userSub.unsubscribe();
  }

  async onDeletePress() {
    if (this.productSelected) {
      if (this.productSelected.new) {
        this.deSelectProduct();
      } else {
        await this.prodServ.deleteProduct(this.productSelected);
        this.deSelectProduct();
      }
    }
  }
  async onSavePress() {
    if (this.productSelected) {
      if (this.productSelected.new) {
        await this.prodServ.createProduct(this.productSelected);
      } else {
        await this.prodServ.updateProduct(this.productSelected);
      }
    }
  }
}
