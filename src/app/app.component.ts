import { Component, OnInit } from '@angular/core';
import { AuthService } from './services/auth.service';
import { AmplifyService } from 'aws-amplify-angular';
import { API, Auth } from 'aws-amplify';

@Component({
  // tslint:disable-next-line
  selector: 'body',
  // templateUrl: './app.component.html'
  template: '<router-outlet></router-outlet>'
})
export class AppComponent {

  signedIn: boolean;
  user: any;
  greeting: string;

  constructor(private amplifyService: AmplifyService) {
    this.amplifyService.authStateChange$
      .subscribe(authState => {
        this.signedIn = authState.state === 'signedIn';
        if (!authState.user) {
          this.user = null;
        } else {
          this.user = authState.user;
          this.greeting = 'Hello ' + this.user.username;
        }
      });
  }

  callApi(): void {
    API.get('table', '/table/products', {}).then((result) => {
      debugger
    })
      .catch((err) => {
        debugger
      });
  }

  onSingOut() {
    Auth.signOut()
      .then(data => console.log(data))
      .catch(err => console.log(err));
  }

}
