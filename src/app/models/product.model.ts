export class ProductModel {
    data: ProductData[];
    meta: any[];
}

export class ProductData {
    id?: number;
    categoryID: number;
    description: string;
    'prod_code': string;
    'prod_name': string;
    quantityPerUnit: string;
    unitPrice: number;
    unitsInStock: number;
    vender: string;
    new?: boolean;
    constructor(isNew: boolean) {
        this.new = isNew;
    }
}