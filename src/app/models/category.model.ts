export class CategoryModel {
    data: CategoryData[];
    meta: any[];
}

export class CategoryData {
    id?: number;
    name: string;
}