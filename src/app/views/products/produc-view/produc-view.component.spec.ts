import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProducViewComponent } from './produc-view.component';

describe('ProducViewComponent', () => {
  let component: ProducViewComponent;
  let fixture: ComponentFixture<ProducViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProducViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProducViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
