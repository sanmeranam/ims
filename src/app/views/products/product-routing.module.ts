import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductListComponent } from './product-list/product-list.component';
import { CategoryComponent } from './category/category.component';
import { ProductService } from 'src/app/services/product.service';

const routes: Routes = [
    {
        path: '',
        component: ProductListComponent,
        data: {
            title: 'Products',
            api: '/table/products'
        },
        resolve: {
            data: ProductService
        }
    },
    {
        path: 'category',
        component: CategoryComponent,
        data: {
            title: 'Product Category',
            api: '/table/category'
        },
        resolve: {
            data: ProductService
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProductRoutingModule { }
