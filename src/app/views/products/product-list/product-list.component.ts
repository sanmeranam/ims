import { Component, OnInit, OnDestroy } from '@angular/core';
import { ColumnMode, SelectionType } from '@swimlane/ngx-datatable';
import { ProductModel } from 'src/app/models/product.model';
import { ProductService } from 'src/app/services/product.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit, OnDestroy {


  ColumnMode = ColumnMode;
  SelectionType = SelectionType;
  selected = [];
  loading = false;
  products: ProductModel;
  sub: Subscription;
  sub2: Subscription;

  constructor(private prodService: ProductService) {
  }

  ngOnInit() {
    this.refreshLoad();
    this.sub2 = this.prodService.selectedProduct.subscribe(p => {
      if (!p) {
        this.selected = [];
      }
    });

  }

  refreshLoad() {
    this.loading = true;
    this.sub = this.prodService.products.subscribe((result: ProductModel) => {
      this.products = result;
      this.loading = false;
    });
  }

  onSelect({ selected }) {
    this.prodService.selectedProduct.next(selected[0]);
  }


  ngOnDestroy(): void {
    this.sub.unsubscribe();
    this.sub2.unsubscribe();
  }
}
