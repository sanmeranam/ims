import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { CategoryModel, CategoryData } from 'src/app/models/category.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit, OnDestroy {

  catgory: CategoryModel;
  sub: Subscription;
  newName: string;
  constructor(private prodServ: ProductService) {
    this.addNew();
  }

  addNew() {
    this.newName = '';
  }

  async saveNew() {
    if (this.newName) {
      await this.prodServ.addCategory(this.newName);
      this.addNew();
    }
  }

  ngOnInit() {
    this.sub = this.prodServ.category.subscribe(list => {
      this.catgory = list;
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }


}
