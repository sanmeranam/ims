import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductListComponent } from './product-list/product-list.component';
import { ProducViewComponent } from './produc-view/produc-view.component';
import { CategoryComponent } from './category/category.component';
import { ProductRoutingModule } from './product-routing.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [ProductListComponent, ProducViewComponent, CategoryComponent],
  imports: [
    FormsModule,
    ProductRoutingModule,
    CommonModule,
    NgxDatatableModule.forRoot({
      messages: {
        emptyMessage: 'No data to display', // Message to show when array is presented, but contains no values
        totalMessage: 'total', // Footer total message
        selectedMessage: 'selected' // Footer selected message
      }
    })
  ]
})
export class ProductsModule { }
