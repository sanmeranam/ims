import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit, OnDestroy {


  user: any;
  credentials = { username: '', password: '' };
  userSub: Subscription;
  error = '';
  isNewRequire = false;
  newPassword = '';
  constructor(private auth: AuthService, private route: Router) {
  }


  ngOnInit(): void {
    this.userSub = this.auth.user.subscribe((user => {
      this.user = user;
      this.checkUserChallenge();
    }));
  }

  checkUserChallenge() {
    if (!this.user) {
      return;
    }
    switch (this.user.challengeName) {
      case 'NEW_PASSWORD_REQUIRED':
        this.isNewRequire = true;
        break;
      case 'MFA_SETUP':
        break;
      case 'ERROR':
        this.credentials.username = '';
        this.credentials.password = '';
        this.error = this.user.message;
        this.auth.user.next(null);
        break;
      default:
        this.route.navigate(['/dashboard']);
    }

  }

  async onNewPasswordUpdate() {
    await this.auth.PasswordUpdate(this.user, this.newPassword);
    this.route.navigate(['/dashboard']);
  }

  onLoginPress() {
    this.auth.SignIn(this.credentials.username, this.credentials.password);
  }

  ngOnDestroy(): void {
    this.userSub.unsubscribe();
  }

}
