var express = require('express')
var bodyParser = require('body-parser')
var awsServerlessExpressMiddleware = require('aws-serverless-express/middleware')
var config = require('./config.json')
var mysql = require('mysql');
var sqlUtil = require('./db.util');

// declare a new express app
var app = express()
app.use(bodyParser.json())
app.use(awsServerlessExpressMiddleware.eventContext())

// Enable CORS for all methods
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()
});

var pool = mysql.createPool(config.db);



app.get('/products', function (req, res) {
  pool.getConnection(function (err, connection) {
    if (err) throw err; // not connected!
    connection.query('SELECT * from products', [req.params.id], function (error, results, fields) {
      connection.release();
      if (error) throw error;
      res.json({
        meta: fields,
        data: results
      });
    });
  });
});

app.get('/products/:id', function (req, res) {
  pool.getConnection(function (err, connection) {
    if (err) throw err; // not connected!
    connection.query('SELECT * from products where id=?', [req.params.id], function (error, results, fields) {
      connection.release();
      if (error) throw error;
      res.json({
        meta: fields,
        data: results
      });
    });
  });
});



app.post('/products/:id', function (req, res) {
  pool.getConnection(function (err, connection) {
    if (err) throw err; // not connected!
    connection.query(sqlUtil.JSONUpdate('products', req.body, req.params.id), function (error, results, fields) {
      connection.release();
      if (error) throw error;
      res.json({
        meta: fields,
        data: results
      });
    });
  });
});



app.put('/products', function (req, res) {
  pool.getConnection(function (err, connection) {
    if (err) throw err; // not connected!

    let InsObj = sqlUtil.JSONInsert('products', req.body);

    connection.query(InsObj.statement, InsObj.values, function (error, results, fields) {
      connection.release();
      if (error) throw error;
      res.json({
        meta: fields,
        data: results
      });
    });
  });
});



app.delete('/products/:id', function (req, res) {
  pool.getConnection(function (err, connection) {
    if (err) throw err; // not connected!
    connection.query('DELETE from products where id=?', [req.params.id], function (error, results, fields) {
      connection.release();
      if (error) throw error;
      res.json({
        meta: fields,
        data: results
      });
    });
  });
});

app.listen(3000, function () {
  console.log("App started")
});


module.exports = app
