module.exports = {
    JSONUpdate: function (table, data, id) {
        let fields = [];
        let values = [];
        fields = Object.keys(data);
        values = fields.map(name => {
            if (typeof (data[name] == "string")) {
                return `${name} = '${data[name]}'`;
            } else {
                return `${name} = ${data[name]}`;
            }
        });

        return `UPDATE ${table} SET ${values.join(',')} where id=${id}`;
    },
    JSONInsert: function (table, data) {
        let fields = [];
        let values = [];
        let endMark = '';
        data = Array.isArray(data) && data.length === 1 ? data[0] : data;

        if (Array.isArray(data)) {
            fields = Object.keys(data[0]);
            data.forEach(row => {
                values.push(fields.map(cname => row[cname]));
            });
            endMark = '?'
            values = [values];
        } else {
            fields = Object.keys(data);
            values = fields.map(cname => data[cname]);
            endMark = `(${fields.map(i => '?')})`;
        }
        console.log(values)
        let statmt = `INSERT INTO ${table}(${fields.join(",")}) VALUES ${endMark}`;

        return {
            statement: statmt,
            values
        }
    }

}