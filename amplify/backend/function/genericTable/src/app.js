var express = require('express')
var bodyParser = require('body-parser')
var awsServerlessExpressMiddleware = require('aws-serverless-express/middleware')
var config = require('./config.json')
var mysql = require('mysql');
var sqlUtil = require('./db.util');

// declare a new express app
var app = express()
app.use(bodyParser.json())
app.use(awsServerlessExpressMiddleware.eventContext())

// Enable CORS for all methods
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  res.header("Access-Control-Allow-Methods", "OPTIONS,POST,GET,PUT,DELETE")
  next()
});

var pool = mysql.createPool(config.db);



app.get('/table/:name', function (req, res) {
  pool.getConnection(function (err, connection) {
    if (err) throw err; // not connected!
    connection.query(`SELECT * from ${req.params.name}`, [req.params.id], function (error, results, fields) {
      connection.release();
      if (error) throw error;
      res.json({
        meta: fields,
        data: results
      });
    });
  });
});

app.get('/table/:name/:id', function (req, res) {
  pool.getConnection(function (err, connection) {
    if (err) throw err; // not connected!
    connection.query(`SELECT * from ${req.params.name} where id=?`, [req.params.id], function (error, results, fields) {
      connection.release();
      if (error) throw error;
      res.json({
        meta: fields,
        data: results
      });
    });
  });
});



app.post('/table/:name/:id', function (req, res) {
  pool.getConnection(function (err, connection) {
    if (err) throw err; // not connected!
    connection.query(sqlUtil.JSONUpdate(req.params.name, req.body, req.params.id), function (error, results, fields) {
      connection.release();
      if (error) throw error;
      res.json({
        meta: fields,
        data: results
      });
    });
  });
});



app.put('/table/:name', function (req, res) {
  pool.getConnection(function (err, connection) {
    if (err) throw err; // not connected!

    let InsObj = sqlUtil.JSONInsert(req.params.name, req.body);

    connection.query(InsObj.statement, InsObj.values, function (error, results, fields) {
      connection.release();
      if (error) throw error;
      res.json({
        meta: fields,
        data: results
      });
    });
  });
});



app.delete('/table/:name/:id', function (req, res) {
  pool.getConnection(function (err, connection) {
    if (err) throw err; // not connected!
    connection.query(`DELETE from ${req.params.name} where id=?`, [req.params.id], function (error, results, fields) {
      connection.release();
      if (error) throw error;
      res.json({
        meta: fields,
        data: results
      });
    });
  });
});

app.listen(3000, function () {
  console.log("App started")
});


module.exports = app
